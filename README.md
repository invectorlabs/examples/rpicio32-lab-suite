# RPICIO32 lab suite

Software used to flash the RPICO32 module ESP-AT firmware

## Getting started

This suite is used to flash the RPICO32 unit with the ESP-AT interpreter from Espressif. 
To run it you simply execute the "flashit" command from a bash shell. It will then tell you what you need to do.

## Comments and updates

The software is very rudimentary but it works and it is easy to change. 
Parts of it will go into the production fixture but for now we are only using it in our lab.

If you're using this software and makes any changes to it, please make your changes by creating a PR.

## Things that need changing for it to run on your PC

There are a lot of references to my username (pontus) in the scripts. Those need to be changed, alternatively it could be made into a parameter or some other intelligent mechanism for detecting attached units.

The serial port used should also probably be taken as an in-parameter to the script to make it more useable.
